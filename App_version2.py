import os
import sys
from PyQt5.QtWidgets import (QApplication, QMainWindow, QWidget, QGraphicsView,
                             QGraphicsScene, QHBoxLayout, QFileDialog,
                             QMessageBox, QLabel, QComboBox, QPushButton)
from PyQt5.QtCore import QDir
from PyQt5.QtGui import QIcon, QPixmap, QFont, QWheelEvent, QImage
import cv2 as cv
#import numpy as np
import Transformaciones as tr


class ventanaPrincipal(QMainWindow):
    def __init__(self):
        super(ventanaPrincipal, self).__init__()

        self.archvo_imagen = ''

        # Titulo de la ventana
        self.tituloVentana()
        # Creación Barra del Menú
        #self.barraMenu()
        # Creación barra de herramientas (la de los íconos)
        self.accionesBasicas()
        # Canvas de la imagen Original
        self.canvasOriginal()
        # Canvas de las transformaciones de la imagen
        self.canvasTransformaciones()
        
        #self.graphicsView1.horizontalScrollBar().
        self.graphicsView1.verticalScrollBar().valueChanged.connect(self.graphicsView2.verticalScrollBar().setValue)
        self.graphicsView1.horizontalScrollBar().valueChanged.connect(self.graphicsView2.horizontalScrollBar().setValue)

        # Creación widget principal
        self.widgetPrincipal()
        # Maximizar Ventana
        self.maximizarVentana()
        

    # Método para dar el nombre a la aplicación
    def tituloVentana(self):
        self.setWindowTitle("Programa Patologia")

    # Metodo para crear la barra de herramientas (la que tiene los íconos)
    def accionesBasicas(self):
        acciones = self.addToolBar("")

        ### Cargar la imagen
        cargar = QPushButton(QIcon('iconos/cargar.svg'), "  Cargar", self)
        cargar.clicked.connect(self.cargarImagen)
        acciones.addWidget(cargar)

        ### Guardar la imagen
        guardar = QPushButton(QIcon('iconos/guardar.svg'), "  Guardar", self)
        guardar.clicked.connect(self.guardarLienzo)
        acciones.addWidget(guardar)

        ### Limpiar lienzo
        limpiar = QPushButton(QIcon('iconos/escoba.svg'), "Limpiar Lienzo", self)
        limpiar.clicked.connect(self.limpiarLianzo)
        acciones.addWidget(limpiar)

        ### Salida Aplicación
        salir = QPushButton(QIcon("iconos/salida.svg"), 'Salir Aplicación', self)
        salir.clicked.connect(self.salidaAplicacion)
        acciones.addWidget(salir)

        ### Agregar un separador
        acciones.addSeparator()

        ### --- Crear el Pop-up menu Mejora contraste ----

        ##### Estilo del texto
        texto = QLabel("Mejoramiento Contraste", self)
        texto.setIndent(5)
        estilo = QFont()
        estilo.setBold(True)
        texto.setFont(estilo)
        acciones.addWidget(texto)

        #### Espacio en blanco
        blanco = QLabel(" ", self)
        blanco.setIndent(10)
        acciones.addWidget(blanco)

        self.combo_box = QComboBox(self)

        lista_transformaciones = [" ", "Balance de Blancos (WB)", 
                                  "Normalizar Tinción (TN)", 
                                  "Mejoramiento de Bordes (S)", "WB + TN + S", 
                                  "Ecualización (E)", 
                                  "Ecualización Adaptativo (AE)", "WB + AE"]
        self.combo_box.addItems(lista_transformaciones)
        self.combo_box.currentIndexChanged.connect(self.transformacionesContraste)
        acciones.addWidget(self.combo_box)
        self.combo_box.setEnabled(False)

        ### Agregar un separador
        acciones.addSeparator()

        ### --- Crear el Pop-up menu Lineales RGB ----

        texto2 = QLabel("Transformaciones Lineales RGB ", self)
        texto2.setIndent(5)
        estilo2 = QFont()
        estilo2.setBold(True)
        texto2.setFont(estilo2)
        acciones.addWidget(texto2)

        #### Espacio en blanco
        blanco2 = QLabel(" ", self)
        blanco2.setIndent(10)
        acciones.addWidget(blanco2)

        self.combo_box2 = QComboBox(self)

        lista_transformaciones2 = [" ", "R(RGB) + WB", "Y(YCrCb) + WB", "C(CMYK) + WB",
                                   "M(CMYK) + WB", "Y(CMYK) + WB", "Espacio XYz + WB",
                                   "U(YUV) + WB", "V(YUV) + WB"]
        self.combo_box2.addItems(lista_transformaciones2)
        self.combo_box2.currentIndexChanged.connect(self.transformacionesLineales)
        acciones.addWidget(self.combo_box2)
        self.combo_box2.setEnabled(False)

        ### Agregar un separador
        acciones.addSeparator()

        ### --- Crear el Pop-up menu No-Lineales ----
        texto3 = QLabel("Transformaciones No Lineales", self)
        texto3.setIndent(5)
        estilo3 = QFont()
        estilo3.setBold(True)
        texto3.setFont(estilo3)
        acciones.addWidget(texto3)

        #### Espacio en blanco
        blanco3 = QLabel(" ", self)
        blanco3.setIndent(10)
        acciones.addWidget(blanco3)

        self.combo_box3 = QComboBox(self)

        lista_transformaciones3 = [" ", "H(HSI) + WB", "S(HSI) + WB", "Max(HMMD) + WB",
                                   "Min(HMMD) + WB", "L(Lab) + WB", "a(Lab) + WB",
                                   "b(Lab) + WB", "u(Luv) + WB", "v(Luv) + WB"]

        self.combo_box3.addItems(lista_transformaciones3)
        self.combo_box3.currentIndexChanged.connect(self.transformacionesNo_lineales)
        acciones.addWidget(self.combo_box3)
        self.combo_box3.setEnabled(False)

    def transformacionesContraste(self, n):

        if n == 0:
            print("")
        elif n == 1:
            self.combo_box.activated.connect(self.balanceBlancos)
        elif n == 2:
            self.combo_box.activated.connect(self.normalizarTincion)
        elif n == 3:
            self.combo_box.activated.connect(self.mejoraBordes)
        elif n == 4:
            self.combo_box.activated.connect(self.tripleAccion)
        elif n == 5:
            self.combo_box.activated.connect(self.ecualizacion)
        elif n == 6:
            self.combo_box.activated.connect(self.ecualizacion_adaptativa)
        elif n == 7:
            self.combo_box.activated.connect(self.balance_adaptativo)
        else:
            print("")

    def transformacionesLineales(self, n):

        if n == 0:
            print("")
        elif n == 1:
            self.combo_box2.activated.connect(self.R_balance)
        elif n == 2:
            self.combo_box2.activated.connect(lambda x: self.YCrCb_balance(0))
        elif n == 3:
            self.combo_box2.activated.connect(lambda x: self.CMYK_balance(0))
        elif n == 4:
            self.combo_box2.activated.connect(lambda x: self.CMYK_balance(1))
        elif n == 5:
            self.combo_box2.activated.connect(lambda x: self.CMYK_balance(2))
        elif n == 6:
            self.combo_box2.activated.connect(self.espacioXyz)
        elif n == 7:
            self.combo_box2.activated.connect(lambda x: self.YUV_balance(0))
        elif n == 8:
            self.combo_box2.activated.connect(lambda x: self.YUV_balance(1))
        else:
            print("")

    def transformacionesNo_lineales(self, n):

        if n == 0:
            print("")
        elif n == 1:
            self.combo_box3.activated.connect(lambda x: self.HSI_balance(0))
        elif n == 2:
            self.combo_box3.activated.connect(lambda x: self.HSI_balance(1))
        elif n == 3:
            self.combo_box3.activated.connect(lambda x: self.HMMD_balance(0))
        elif n == 4:
            self.combo_box3.activated.connect(lambda x: self.HMMD_balance(1))
        elif n == 5:
            self.combo_box3.activated.connect(lambda x: self.Lab_balance(0))
        elif n == 6:
            self.combo_box3.activated.connect(lambda x: self.Lab_balance(1))
        elif n == 7:
            self.combo_box3.activated.connect(lambda x: self.Lab_balance(2))
        elif n == 8:
            self.combo_box3.activated.connect(lambda x: self.Luv_balance(0))
        elif n == 9:
            self.combo_box3.activated.connect(lambda x: self.Luv_balance(1))
        else:
            print("")

    # Método que permite cargar una imagen con formatos png, jpg y bmp
    def cargarImagen(self):
        self.archvo_imagen, self.extension_archivo = QFileDialog.getOpenFileName(self, "Abrir Archivo", QDir.currentPath(), "Imágenes (*.png *.xpm *.jpg *.bmp)")
        if self.archvo_imagen != '':
            self.combo_box.setEnabled(True)
            self.combo_box2.setEnabled(True)
            self.combo_box3.setEnabled(True)
            self.escena1.clear()
            self.escena2.clear()
            self.Imagen_RGB = cv.cvtColor(cv.imread(str(self.archvo_imagen)),
                                          cv.COLOR_BGR2RGB)
            # [self.fil, self.col, self.ch] = np.shape(self.Imagen_RGB)

            self.imagen_original = QPixmap(self.archvo_imagen)
            self.escena1.addPixmap(self.imagen_original)
            self.escena2.addPixmap(self.imagen_original)

    def guardarLienzo(self):
        if self.archvo_imagen != '':
            directorio, formato = QFileDialog.getSaveFileName(self, "Guardar Archivo", 
                                                              QDir.currentPath(), 
                                                              "PNG(*.png);; " 
                                                              "JPG(*.jpg);;" 
                                                              "BMP(*.bmp)")
            if directorio == '':
                pass
            else:
                try:
                    cv.imwrite(directorio, cv.cvtColor(self.imagenParaGuardar, 
                                                       cv.COLOR_BGR2RGB))
                except:
                    print(directorio)
        else:
            QMessageBox.warning(None, "Atención", 
                                "Recuerde que primero debe cargar la imagen antes de" 
                                "guardarla")

    # Método para limpiar los lienzos
    def limpiarLianzo(self):
        self.escena1.clear()
        self.escena2.clear()
        self.combo_box.setEnabled(False)
        self.combo_box2.setEnabled(False)
        self.combo_box3.setEnabled(False)

    # Método para salir de la aplicación en donde se incluye un mensaje de advertencia
    def salidaAplicacion(self):
        ret = QMessageBox.question(self, self.tr("Aplicacion Patología"),
                                   self.tr("Vas a salir del programa.\n" + 
                                           "Deseas Salir?"),
                                   QMessageBox.Yes | QMessageBox.No)
        if ret == QMessageBox.Yes:
            QApplication.exit()



    # Método para crear el lienzo de la izquierda. En este se mostrará siempre la imagen original
    def canvasOriginal(self):
        self.graphicsView1 = QGraphicsView(self)
        self.escena1 = QGraphicsScene(self)
        self.graphicsView1.setScene(self.escena1)

    # Método para crear el lienzo de la derecha. En este se mostrarán las transformaciones
    def canvasTransformaciones(self):
        self.graphicsView2 = QGraphicsView(self)
        self.escena2 = QGraphicsScene(self)
        self.graphicsView2.setScene(self.escena2)
        # Desactivar el scroll de esta ventana
        # self.graphicsView2.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        # self.graphicsView2.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

    # Método que permite posicionar los lienzos en la ventana principal
    def widgetPrincipal(self):
        self.central = QWidget()
        self.setCentralWidget(self.central)
        self.campo = QHBoxLayout(self.central)
        self.campo.addWidget(self.graphicsView1)
        self.campo.addWidget(self.graphicsView2)

    # Método que permite maximizar la ventana principal una vez inicie la aplicación
    def maximizarVentana(self):
        QWidget.showMaximized(self)

    # ----------************* transformaciones **********--------------------------
    def balanceBlancos(self):
        if self.archvo_imagen != '':
            try:
                Ex = tr.white_balance(self.Imagen_RGB)
                height, width, channel = self.Imagen_RGB.shape
                bytesPerLine = 3 * width
                qImg = QImage(Ex.data, width, height, bytesPerLine, QImage.Format_RGB888)
                self.escena2.addPixmap(QPixmap.fromImage(qImg))
            except:
                print("Fallo al aplicar balance de blancos")
            # self.escena2.clear()
            #print(qImg)
        #    try:
        #        del self.imagenParaGuardar
        #    except:
        #        pass
        #    imagen_BB = tr.white_balance(self.Imagen_RGB)
        #    self.imagenParaGuardar = imagen_BB
        #    cv.imwrite('imagen.png', cv.cvtColor(imagen_BB, cv.COLOR_BGR2RGB))
        #    imagen_BB = QPixmap('imagen.png')
        #    self.escena2.addPixmap(imagen_BB)
        #    os.remove('imagen.png')

    def normalizarTincion(self):
        if self.archvo_imagen != '':
            #try:
            #    Ex = self.Imagen_RGB
            #    height, width, channel = self.Imagen_RGB.shape
            #    bytesPerLine = 3 * width
            #    qImg = QImage(Ex.data, width, height, bytesPerLine, QImage.Format_RGB888)
            #    self.escena2.addPixmap(QPixmap.fromImage(qImg))
            #except:
            #    print("Nanay")
             self.escena2.clear()
             imagen_BB = tr.normalizeStaining(self.Imagen_RGB)
             self.imagenParaGuardar = imagen_BB
             cv.imwrite('imagen.png', cv.cvtColor(imagen_BB, cv.COLOR_BGR2RGB))
             imagen_BB = QPixmap('imagen.png')
             self.escena2.addPixmap(imagen_BB)
             os.remove('imagen.png')

    def mejoraBordes(self):
        if self.archvo_imagen != '':
            self.escena2.clear()
            imagen_BB = tr.bordes_contraste(self.Imagen_RGB)
            self.imagenParaGuardar = imagen_BB
            cv.imwrite('imagen.png', cv.cvtColor(imagen_BB, cv.COLOR_BGR2RGB))
            imagen_BB = QPixmap('imagen.png')
            self.escena2.addPixmap(imagen_BB)
            os.remove('imagen.png')

    def tripleAccion(self):
        if self.archvo_imagen != '':
            self.escena2.clear()
            imagen_BB = tr.normalizeStaining(self.Imagen_RGB)
            imagen_BB = tr.white_balance(imagen_BB)
            imagen_BB = tr.bordes_contraste(imagen_BB)
            self.imagenParaGuardar = imagen_BB
            cv.imwrite('imagen.png', cv.cvtColor(imagen_BB, cv.COLOR_BGR2RGB))
            imagen_BB = QPixmap('imagen.png')
            self.escena2.addPixmap(imagen_BB)
            os.remove('imagen.png')

    def ecualizacion(self):
        if self.archvo_imagen != '':
            self.escena2.clear()
            imagen_BB = tr.histograma(self.Imagen_RGB)
            self.imagenParaGuardar = imagen_BB
            cv.imwrite('imagen.png', cv.cvtColor(imagen_BB, cv.COLOR_BGR2RGB))
            imagen_BB = QPixmap('imagen.png')
            self.escena2.addPixmap(imagen_BB)
            os.remove('imagen.png')

    def ecualizacion_adaptativa(self):
        if self.archvo_imagen != '':
            self.escena2.clear()
            imagen_BB = tr.histograma_adaptativo(self.Imagen_RGB)
            self.imagenParaGuardar = imagen_BB
            cv.imwrite('imagen.png', cv.cvtColor(imagen_BB, cv.COLOR_BGR2RGB))
            imagen_BB = QPixmap('imagen.png')
            self.escena2.addPixmap(imagen_BB)
            os.remove('imagen.png')

    def balance_adaptativo(self):
        if self.archvo_imagen != '':
            self.escena2.clear()
            imagen_BB = tr.white_balance(self.Imagen_RGB)
            imagen_BB = tr.histograma_adaptativo(imagen_BB)
            self.imagenParaGuardar = imagen_BB
            cv.imwrite('imagen.png', cv.cvtColor(imagen_BB, cv.COLOR_BGR2RGB))
            imagen_BB = QPixmap('imagen.png')
            self.escena2.addPixmap(imagen_BB)
            os.remove('imagen.png')

    def espacioXyz(self):
        if self.archvo_imagen != '':
            self.escena2.clear()
            imagen_BB = tr.Xyz_balance(self.Imagen_RGB)
            self.imagenParaGuardar = imagen_BB
            cv.imwrite('imagen.png', cv.cvtColor(imagen_BB, cv.COLOR_BGR2RGB))
            imagen_BB = QPixmap('imagen.png')
            self.escena2.addPixmap(imagen_BB)
            os.remove('imagen.png')

    def R_balance(self):
        if self.archvo_imagen != '':
            self.escena2.clear()
            R, _, _ = cv.split(self.Imagen_RGB)
            imagen_BB = tr.balance_Bla_canal(R)
            self.imagenParaGuardar = imagen_BB
            cv.imwrite('imagen.png', imagen_BB)
            imagen_BB = QPixmap('imagen.png')
            self.escena2.addPixmap(imagen_BB)
            os.remove('imagen.png')

    def YCrCb_balance(self, n):
        if self.archvo_imagen != '':
            self.escena2.clear()
            imagen_BB = tr.Canal_YCrCb(self.Imagen_RGB, n)
            self.imagenParaGuardar = imagen_BB
            cv.imwrite('imagen.png', imagen_BB)
            imagen_BB = QPixmap('imagen.png')
            self.escena2.addPixmap(imagen_BB)
            os.remove('imagen.png')

    def CMYK_balance(self, n):
        if self.archvo_imagen != '':
            self.escena2.clear()
            imagen_BB = tr.CMY(self.Imagen_RGB, n)
            self.imagenParaGuardar = imagen_BB
            cv.imwrite('imagen.png', imagen_BB)
            imagen_BB = QPixmap('imagen.png')
            self.escena2.addPixmap(imagen_BB)
            os.remove('imagen.png')

    def YUV_balance(self, n):
        if self.archvo_imagen != '':
            self.escena2.clear()
            imagen_BB = tr.YUV(self.Imagen_RGB, n)
            self.imagenParaGuardar = imagen_BB
            cv.imwrite('imagen.png', imagen_BB)
            imagen_BB = QPixmap('imagen.png')
            self.escena2.addPixmap(imagen_BB)
            os.remove('imagen.png')

    def HSI_balance(self, n):
        if self.archvo_imagen != '':
            self.escena2.clear()
            imagen_BB = tr.HSI(self.Imagen_RGB, n)
            self.imagenParaGuardar = imagen_BB
            cv.imwrite('imagen.png', imagen_BB)
            imagen_BB = QPixmap('imagen.png')
            self.escena2.addPixmap(imagen_BB)
            os.remove('imagen.png')

    def HMMD_balance(self, n):
        if self.archvo_imagen != '':
            self.escena2.clear()
            imagen_BB = tr.HMMD(self.Imagen_RGB, n)
            self.imagenParaGuardar = imagen_BB
            cv.imwrite('imagen.png', imagen_BB)
            imagen_BB = QPixmap('imagen.png')
            self.escena2.addPixmap(imagen_BB)
            os.remove('imagen.png')

    def Lab_balance(self, n):
        if self.archvo_imagen != '':
            self.escena2.clear()
            imagen_BB = tr.Lab(self.Imagen_RGB, n)
            self.imagenParaGuardar = imagen_BB
            cv.imwrite('imagen.png', imagen_BB)
            imagen_BB = QPixmap('imagen.png')
            self.escena2.addPixmap(imagen_BB)
            os.remove('imagen.png')

    def Luv_balance(self, n):
        if self.archvo_imagen != '':
            self.escena2.clear()
            imagen_BB = tr.Luv(self.Imagen_RGB, n)
            self.imagenParaGuardar = imagen_BB
            cv.imwrite('imagen.png', imagen_BB)
            imagen_BB = QPixmap('imagen.png')
            self.escena2.addPixmap(imagen_BB)
            os.remove('imagen.png')

    def wheelEvent(self, event: QWheelEvent):
        try:
            if self.archvo_imagen == '':
                pass
            else:
                escala = (event.angleDelta().y() / 120) * 0.1
                self.graphicsView1.scale(escala + 1, escala + 1)
                self.graphicsView2.scale(escala + 1, escala + 1)
            # self.escena2.wheelEvent()
            # numDegrees = event.delta() / 8
            # numSteps = numDegrees / 15
            # event.accept()
        except:
            print("Fallo al procesar el evento de giro de la rueda del ratón")
            

    # if event->orientation() == Qt.Horizontal:
    #    scrollHorizontally(numSteps)
    # else:
    #    scrollVertically(numSteps)


aplicacion = QApplication(sys.argv)
ventana = ventanaPrincipal()
ventana.show()
aplicacion.exec_()
